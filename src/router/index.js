import Vue from 'vue'
import VueRouter from 'vue-router'
// 导入登录组件
import Login from '.././components/Login.vue'
//导入主页面组件
import Home from '.././components/Home.vue'
//导入欢迎页面
import Welcome from '.././components/Welcome.vue'
//导入用户管理组件
import Users from '.././components/User/Users.vue'
//导入权限列表
import Rights from '.././components/power/Rights.vue'
import Roles from '.././components/power/Roles.vue'

//导入商品分类的组件
import Categories from '.././components/goods/Categories.vue'

//导入商品数据的组件
import Parmas from '.././components/goods/Params.vue'
import GoodsList from '.././components/goods/List.vue'
import AddGoods from '.././components/goods/AddGoods.vue'
import Orders from '.././components/Order/Orders.vue'

//数据报表的组件
import Reports from '.././components/report/Report.vue'

Vue.use(VueRouter)

const routes = [
    { path: '/', redirect: '/login' },
    { path: '/login', component: Login },
    {
        path: '/home',
        component: Home,
        //因为welcome页面是在home页面显示的所以是在它的children中写，并且重定向为welcome页面
        redirect: '/welcome',
        children: [
            { path: '/welcome', component: Welcome },
            { path: '/users', component: Users },
            { path: '/rights', component: Rights },
            { path: '/roles', component: Roles },
            { path: '/categories', component: Categories },
            { path: '/params', component: Parmas },
            { path: '/goods', component: GoodsList },
            { path: '/goods/add', component: AddGoods },
            { path: '/orders', component: Orders },
            { path: '/reports', component: Reports }
        ]
    }
]

const router = new VueRouter({
    routes
})

//挂载路由导航守卫
router.beforeEach((to, from, next) => {
    //to 将要访问的路径
    //from 代表从哪个路径跳转而来
    //next 是一个函数，表示放行
    //next()放行  next('/login)为强制跳转

    if (to.path === '/login') return next();
    //获取token值，值为空则跳转到login页面
    const token = window.sessionStorage.getItem('token')
    if (!token) return this.$router.push('/login')
    next();
})

export default router